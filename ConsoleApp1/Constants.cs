﻿using System;

namespace ConsoleApp1
{
    public static class Constants
    {
        public static double NETWORK_OFFSET = 5.0;    //смещение сети по иксу при инициализации
        public static double NETWORK_SPEED = 0.1;  //скорость движения сети
        public static int NUMBER_OF_NETWORKS = 2;      //количество сетей

        public static double MAX_ANGLE = 2 * Math.PI;      //угол нужен для направления движения сети и узла, в радианах, поэтому 2пи

        public static double MIN_RADIUS = 1.0; //мин. радиус передачи
        public static double MAX_RADIUS = 1.0; //макс радиус передачи

        public static double MAX_SPEED = 1.0;  //максимальная скорость узла
        public static double MIN_SPEED = 1.0;  //минимальная скорость движения узла (не считая скорости сети)

        public static int NUMBER_OF_VERTEXES = 10;     //количество узлов

        public static double MIN_HEIGHT = 10;     //мин. высота
        public static double MAX_HEIGHT = 20; //макс. высота
        public static double MIN_WIDTH = 10;  //мин. ширина
        public static double MAX_WIDTH = 20; //макс. ширина

        public static int TIME_TO_LIVE = 20;  //время жизни сообщения

        public static int NUMBER_OF_TRIES = 100; //количество повторений цикла
    }
}