﻿using System;

namespace ConsoleApp1
{
    public static class Randomizer
    {
        private static Random random = new Random();

        public static double GetRandomDouble(double min, double max)
        {
            double randomDouble = random.NextDouble() * (max - min) + min;
            return randomDouble;
        }

        public static int GetRandomInt(int min, int max)
        {
            int randomInt = random.Next(min, max);
            return randomInt;
        }
    }
}