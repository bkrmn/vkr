﻿using System;

namespace ConsoleApp1
{
    public class Network
    {
        private double xMin;
        private double yMin;
        private double xMax;
        private double yMax;
        private double angleInRadians;
        private double speed;
        public Node[] nodeArray;

        public Network(int number)
        {
            this.xMin = Constants.NETWORK_OFFSET * number;      //0 сеть начнется с 0 по иксу, 2 - с 2 смещений по иксу, 10 - с 10 смещений по иксу
            this.yMin = 0;
            this.xMax = this.xMin + Randomizer.GetRandomDouble(Constants.MIN_WIDTH, Constants.MAX_WIDTH);
            this.yMax = this.yMin + Randomizer.GetRandomDouble(Constants.MIN_HEIGHT, Constants.MAX_HEIGHT);
            this.speed = Constants.NETWORK_SPEED;
            this.angleInRadians = Randomizer.GetRandomDouble(0.0, Constants.MAX_ANGLE);
            this.nodeArray = new Node[Constants.NUMBER_OF_VERTEXES];
            for (int i = 0; i < Constants.NUMBER_OF_VERTEXES; i++)
                this.nodeArray[i] = new Node(number, xMin, yMin, xMax, yMax, i);
        }

        public void PreformStepNetwork()        //двигаем сеть
        {
            //смещения краев сети
            this.xMin += this.speed * Math.Cos(this.angleInRadians);
            this.yMin += this.speed * Math.Sin(this.angleInRadians);
            this.xMax += this.speed * Math.Cos(this.angleInRadians);
            this.yMax += this.speed * Math.Sin(this.angleInRadians);
            for (int i = 0; i < Constants.NUMBER_OF_VERTEXES; i++)
            {
                this.nodeArray[i].Move(this.speed * Math.Cos(this.angleInRadians), this.speed * Math.Sin(this.angleInRadians));     //двигаем узлы сети на смещение сети
                this.nodeArray[i].PreformStep(this.xMin, this.yMin, this.xMax, this.yMax);      //двигаем каждый узел сети в соответствии с его скоростью и направлением
            }
        }

        public Node[] GetNodes()
        {
            return this.nodeArray;
        }

        public void PrintNetwork()      //вывод сети на экран, выводит ее границы в данный момент и координаты узлов со значениями сигналов
        {
            Console.Out.WriteLine("xMin = " + Math.Round(this.xMin, 2) + "; xMax = " + Math.Round(this.xMax, 2) + 
                "; yMin = " + Math.Round(this.yMin, 2) + "; yMax = " + Math.Round(this.yMax, 2));
            for (int i = 0; i < Constants.NUMBER_OF_VERTEXES; i++)
            {
                Console.Out.Write("(" + Math.Round(this.nodeArray[i].GetX(), 2) + "; " + Math.Round(this.nodeArray[i].GetY(), 2) + "; " + this.nodeArray[i].GetSignal() + ") ");
            }
            Console.WriteLine();
        }
    }
}
