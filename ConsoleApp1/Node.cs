﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class Node
    {
        private double speed;
        private double angleInRadians;
        private double X;
        private double Y;
        private bool signal;
        private int network;
        private int number;
        private double radius;
        private List<int> path;

        public Node(int network, double xMin, double yMin, double xMax, double yMax, int number)    //конструктор узла сети
        {
            this.X = Randomizer.GetRandomDouble(xMin, xMax);
            this.Y = Randomizer.GetRandomDouble(yMin, yMax);
            this.speed = Randomizer.GetRandomDouble(Constants.MIN_SPEED, Constants.MAX_SPEED);
            this.angleInRadians = Randomizer.GetRandomDouble(0.0, Constants.MAX_ANGLE);
            this.signal = false;
            this.network = network;
            this.number = number;
            this.radius = Randomizer.GetRandomDouble(Constants.MIN_RADIUS, Constants.MAX_RADIUS);
            path = new List<int>(0);
        }

        public void PreformStep(double xMin, double yMin, double xMax, double yMax) //шаг узла сети
        {
            while ((this.X + this.speed * Math.Cos(this.angleInRadians) >= xMax) || //проверка на не выход за пределы сети при следующем шаге
                   (this.Y + this.speed * Math.Sin(this.angleInRadians) >= yMax) ||
                   (this.X + this.speed * Math.Cos(this.angleInRadians) <= xMin) ||
                   (this.Y + this.speed * Math.Sin(this.angleInRadians) <= yMin))
            {
                ChangeDirection();  //изменение направления, если возможен выход за границы
            }
            this.X += this.speed * Math.Cos(this.angleInRadians);
            this.Y += this.speed * Math.Sin(this.angleInRadians);
        }

        private void ChangeDirection()      //изменение направления и скорости
        {
            this.angleInRadians = Randomizer.GetRandomDouble(0.0, Constants.MAX_ANGLE);
            this.speed = Randomizer.GetRandomDouble(Constants.MIN_SPEED, Constants.MAX_SPEED);
        }

        public void Move(double x, double y)        //сделано для изменения координат в согласии с движением самой сети
        {
            this.X += x;
            this.Y += y;
        }

        public double GetX()
        {
            return this.X;
        }

        public double GetY()
        {
            return this.Y;
        }

        public bool GetSignal()
        {
            return this.signal;
        }

        public double GetRadius()
        {
            return this.radius;
        }

        public List<int> GetPath()
        {
            return this.path;
        }

        public void SetSignal(bool value)
        {
            this.signal = value;
        }

        public void SetSignal(bool value, List<int> path)
        {
            this.signal = value;
            path.Add(this.network);
            this.path = path;
        }
    }
}