﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Application excelApp = new Application()
            {
                Visible = false
            };
            _Workbook workbook = excelApp.Workbooks.Add("");
            _Worksheet worksheet = (_Worksheet)workbook.ActiveSheet;

            worksheet.Cells[1, 1] = "Количество вершин";
            worksheet.Cells[1, 2] = "Количество сетей";
            worksheet.Cells[1, 3] = "Смещение сети";
            worksheet.Cells[1, 4] = "Скорость движения сети";
            worksheet.Cells[1, 5] = "Средняя ширина сети";
            worksheet.Cells[1, 6] = "Средняя длина сети";
            worksheet.Cells[1, 7] = "Средний радиус (для ориентированных графов)";
            worksheet.Cells[1, 8] = "Средняя скорость движения узлов";
            worksheet.Cells[1, 9] = "Время жизни сообщения";
            worksheet.Cells[1, 10] = "Среднее время доставки";
            worksheet.Cells[1, 11] = "Доля удачных передач";
            worksheet.Cells[1, 12] = "Средняя длина пути";
            worksheet.Cells[1, 13] = "Количество частичных путей";
            worksheet.Cells[1, 14] = "Длина частичного пути";
            for (int i = 15; i < 25; i++)
                worksheet.Cells[1, i] = "Сеть " + (i - 15);

            int iter = 2;
            for (; Constants.NUMBER_OF_VERTEXES <= 100; Constants.NUMBER_OF_VERTEXES += 10)
            {
                double meanTime = 0.0;
                double meanPathLength = 0.0;
                int successCount = 0;
                double meanPartialPathLength = 0.0;
                int partialPathsCount = 0;
                int[] usedNodes = new int[Constants.NUMBER_OF_NETWORKS];
                for (int i = 0; i < usedNodes.Length; i++)
                    usedNodes[i] = 0;
                for (int numberOfTry = 0; numberOfTry < Constants.NUMBER_OF_TRIES; numberOfTry++)
                {
                    Network[] nwArray = new Network[Constants.NUMBER_OF_NETWORKS];      //массив сетей
                    for (int i = 0; i < Constants.NUMBER_OF_NETWORKS; i++)
                    {
                        nwArray[i] = new Network(i);
                    }
                    //даем сигнал случайной вершине и выбираем конечную
                    int startNode = 0;
                    int lastNode = 0;
                    while (startNode == lastNode)
                    {
                        startNode = Randomizer.GetRandomInt(0, Constants.NUMBER_OF_VERTEXES);
                        lastNode = Randomizer.GetRandomInt(0, Constants.NUMBER_OF_VERTEXES);
                    }
                    nwArray[0].nodeArray[startNode].SetSignal(true);    //даем сигнал стартовой вершине
                    int timeCounter = 0;
                    Node[] nodeArray = new Node[0];
                    while ((timeCounter < Constants.TIME_TO_LIVE) &&                 //пока время жизни сигнала не истекло и сигнал не доставлен
                        (nwArray[Constants.NUMBER_OF_NETWORKS - 1].nodeArray[lastNode].GetSignal() == false))
                    {
                        nodeArray = new Node[0];
                        for (int i = 0; i < Constants.NUMBER_OF_NETWORKS; i++)
                        {
                            nwArray[i].PreformStepNetwork();        //двигаем сеть (вместе с вершинами)
                            nodeArray = nodeArray.Concat(nwArray[i].GetNodes()).ToArray();      //собираем все вершины в массив для передачи между ними сигнала
                        }
                        List<Tuple<int, int>> newSignals = new List<Tuple<int, int>>(0);    //запоминаем, кому ставить сигнал, чтобы не было вложенности передачи (только соседям)
                        for (int i = 0; i < nodeArray.Length; i++)
                        {
                            if (nodeArray[i].GetSignal() == true)
                            {
                                for (int j = 0; j < nodeArray.Length; j++)
                                {
                                    if (nodeArray[j].GetSignal() == false && i != j)    //если вершина, подозрительная на соседа, не получала сигнал и не та же, от которой передаем
                                    {
                                        if (Math.Sqrt(Math.Pow(nodeArray[i].GetX() - nodeArray[j].GetX(), 2) +      //если расстояние между ними меньше радиуса передачи
                                            Math.Pow(nodeArray[i].GetY() - nodeArray[j].GetY(), 2)) < nodeArray[i].GetRadius())
                                            newSignals.Add(Tuple.Create(i, j));     //запоминаем вершину передачи и получателя
                                    }
                                }
                            }
                        }
                        List<int> alreadySet = new List<int>(0);
                        for (int i = 0; i < newSignals.Count; i++)
                        {
                            if (!alreadySet.Contains(newSignals[i].Item2))
                            {
                                nodeArray[newSignals[i].Item2].SetSignal(true, nodeArray[newSignals[i].Item1].GetPath());       //устанавливаем флаг сигнала в true и заставляем запомнить путь 
                                alreadySet.Add(newSignals[i].Item2);
                            }
                        }

                        timeCounter++;
                    }
                    if (nwArray[Constants.NUMBER_OF_NETWORKS - 1].nodeArray[lastNode].GetSignal() == true)
                    {
                        successCount++;
                        List<int> path = nwArray[Constants.NUMBER_OF_NETWORKS - 1].nodeArray[lastNode].GetPath();
                        int[] usedNodesCurrent = GetUsedNodesCount(path);
                        for (int i = 0; i < usedNodes.Length; i++)
                            usedNodes[i] += usedNodesCurrent[i];
                        int count = GetPartialPathsCount(path);
                        partialPathsCount += count;
                        meanPartialPathLength += 1.0 * path.Count / count;
                        meanPathLength += path.Count;
                        meanTime += timeCounter;
                    }
                } 
                worksheet.Cells[iter, 1] = Constants.NUMBER_OF_VERTEXES;
                worksheet.Cells[iter, 2] = Constants.NUMBER_OF_NETWORKS;
                worksheet.Cells[iter, 3] = Constants.NETWORK_OFFSET;
                worksheet.Cells[iter, 4] = Constants.NETWORK_SPEED;
                worksheet.Cells[iter, 5] = (Constants.MIN_WIDTH + Constants.MAX_WIDTH) / 2;
                worksheet.Cells[iter, 6] = (Constants.MAX_HEIGHT + Constants.MIN_HEIGHT) / 2;
                if (Constants.MAX_RADIUS == Constants.MIN_RADIUS)
                    worksheet.Cells[iter, 7] = (Constants.MAX_RADIUS + Constants.MIN_RADIUS) / 2;
                worksheet.Cells[iter, 8] = (Constants.MAX_SPEED + Constants.MIN_SPEED) / 2;
                worksheet.Cells[iter, 9] = Constants.TIME_TO_LIVE;
                worksheet.Cells[iter, 10] = meanTime / successCount;
                worksheet.Cells[iter, 11] = 1.0 * successCount / Constants.NUMBER_OF_TRIES;
                worksheet.Cells[iter, 12] = 1.0 * meanPathLength / successCount;
                worksheet.Cells[iter, 13] = 1.0 * partialPathsCount / successCount;
                worksheet.Cells[iter, 14] = meanPartialPathLength / successCount;
                for (int i = 0; i < usedNodes.Length; i++)
                    worksheet.Cells[iter, 15 + i] = (1.0 * usedNodes[i] / successCount);
                iter++;
            }
            excelApp.UserControl = false;
            workbook.SaveAs("d:\\aaa.xlsx", XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            workbook.Close();
            Console.ReadLine();
        }

        private static int[] GetUsedNodesCount(List<int> fullPath)
        {
            int[] usedNodesCount = new int[Constants.NUMBER_OF_NETWORKS];
            for (int i = 0; i < usedNodesCount.Length; i++)
                usedNodesCount[i] = 0;
            for (int i = 0; i < fullPath.Count; i++)
                usedNodesCount[fullPath[i]]++;
            return usedNodesCount;
        }

        private static int GetPartialPathsCount(List<int> fullPath)
        {
            int currentNetwork = fullPath[0];
            int currentPathLength = 1;
            int pathsCount = 1;
            for (int i = 1; i < fullPath.Count; i++)
            {
                if (fullPath[i] != currentNetwork)
                {
                    currentNetwork = fullPath[i];
                    pathsCount++;
                }
                else
                    currentPathLength++;
            }
            return pathsCount;
        }
    }
}